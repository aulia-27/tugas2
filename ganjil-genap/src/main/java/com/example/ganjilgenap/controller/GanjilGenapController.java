package com.example.ganjilgenap.controller;

import com.example.ganjilgenap.model.GanjilGenap;
import com.example.ganjilgenap.service.GanjilGenapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class GanjilGenapController {
    GanjilGenap ganjilGenap = new GanjilGenap();

    @Autowired
    private GanjilGenapService service;

    @RequestMapping("/")
    public String getGanjilGenap(Model model){
        model.addAttribute("ganjilGenap", ganjilGenap);
        return "index";
    }

    @RequestMapping(value = "/", params = "cari", method = RequestMethod.POST)
    public String cari(@ModelAttribute("ganjilGenap") GanjilGenap ganjilGenap, Model model){
        model.addAttribute("hasil", service.ganjilGenapProcess(ganjilGenap));
        return "index";
    }
}
