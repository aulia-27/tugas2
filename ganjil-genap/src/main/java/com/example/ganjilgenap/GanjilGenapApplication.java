package com.example.ganjilgenap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GanjilGenapApplication {

    public static void main(String[] args) {
        SpringApplication.run(GanjilGenapApplication.class, args);
    }

}
