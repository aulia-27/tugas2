package com.example.ganjilgenap.service;

import com.example.ganjilgenap.model.GanjilGenap;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GanjilGenapService {
    public List<String> ganjilGenapProcess(GanjilGenap ganjilGenap){
        List<String> result = new ArrayList<>();
        int a = ganjilGenap.getA();
        int b = ganjilGenap.getB();
        for (int i = a; i <= b; i++) {
            if (i % 2 == 0){
                result.add("Angka " + i + " Adalah Ganjil");
            } else {
                result.add("Angka " + i + " Adalah Genap");
            }
        }
        return result;
    }
}
