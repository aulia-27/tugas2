package com.example.hitungvokal.model;

public class HitungVokal {
    private String input;

    public HitungVokal() {
    }

    public HitungVokal(String input) {
        this.input = input;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
