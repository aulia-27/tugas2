package com.example.hitungvokal.controller;

import com.example.hitungvokal.model.HitungVokal;
import com.example.hitungvokal.service.HitungVokalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HitungVokalController {
    HitungVokal hitungVokal = new HitungVokal();

    @Autowired
    private HitungVokalService service;

    @RequestMapping("/")
    public String getHitungVokal(Model model){
        model.addAttribute("hitungVokal", hitungVokal);
        return "index";
    }

    @RequestMapping(value = "/", params = "cari", method = RequestMethod.POST)
    public String cari(@ModelAttribute("hitungVokal") HitungVokal hitungVokal, Model model){
        model.addAttribute("hasil", service.hitungVokal(hitungVokal));
        return "index";
    }

}
