package com.example.hitungvokal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HitungVokalApplication {

    public static void main(String[] args) {
        SpringApplication.run(HitungVokalApplication.class, args);
    }

}
