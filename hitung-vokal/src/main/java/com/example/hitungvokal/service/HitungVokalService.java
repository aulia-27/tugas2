package com.example.hitungvokal.service;

import com.example.hitungvokal.model.HitungVokal;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class HitungVokalService {
    public String[] hitungVokal(HitungVokal model){
        int x = 0;
        boolean A = false, I = false, U = false, E = false, O = false;
        String kalimat = model.getInput();
        String[] str = new String[5];
        String[] output = new String[2];
        StringBuilder out = null;

        for (int i=0; i<kalimat.length(); i++){
            if(kalimat.charAt(i)=='<'){
                kalimat = kalimat.replace("<", "x");
                kalimat = kalimat.replace(">", "x");
                output[1] = "!!!!!!!!!Kemungkinan mau melakukan XSS(<script>)!!!!!!!!!!";
            }
        }

        for(int i=0; i<kalimat.length(); i++){
            if(!A){
                if(kalimat.charAt(i)=='a'||kalimat.charAt(i)=='A'){
                    str[x] = "a";
                    x++;
                    A=true;
                }
            }
            if(!I){
                if(kalimat.charAt(i)=='i'||kalimat.charAt(i)=='I'){
                    str[x] = "i";
                    x++;
                    I=true;
                }
            }
            if(!U){
                if(kalimat.charAt(i)=='u'||kalimat.charAt(i)=='U'){
                    str[x] = "u";
                    x++;
                    U=true;
                }
            }
            if(!E){
                if(kalimat.charAt(i)=='e'||kalimat.charAt(i)=='E'){
                    str[x] = "e";
                    x++;
                    E=true;
                }
            }
            if(!O){
                if(kalimat.charAt(i)=='o'||kalimat.charAt(i)=='O'){
                    str[x] = "o";
                    x++;
                    O=true;
                }
            }
        }
        for(int i=0; i<str.length; i++){
            if(str[i]!=null){
                if(i==0){
                    out = new StringBuilder(str[i]);
                }else{
                    Objects.requireNonNull(out).append(" dan ").append(str[i]);
                }
            }
        }
        output[0] = '"'+kalimat+'"'+" = "+ x +" yaitu "+Objects.requireNonNull(out).toString();
        return output;
    }
}
