package com.example.kalkulatorsederhana.service;

import com.example.kalkulatorsederhana.model.Kalkulator;
import org.springframework.stereotype.Service;

@Service
public class KalkulatorService {
    public int tambah(Kalkulator kalkulator){
        return kalkulator.getA() + kalkulator.getB();
    }

    public int kurang(Kalkulator kalkulator){
        return kalkulator.getA() - kalkulator.getB();
    }

    public int kali(Kalkulator kalkulator){
        return kalkulator.getA() * kalkulator.getB();
    }

    public String bagi(Kalkulator kalkulator){
        String ouput;
        float result;
        if (kalkulator.getB() == 0){
            ouput = "Tidak Bisa Dibagi 0";
        } else {
            result = kalkulator.getA() / kalkulator.getB();
            ouput = String.valueOf(result);
        }
        return ouput;
    }
}
