package com.example.kalkulatorsederhana.controller;

import com.example.kalkulatorsederhana.model.Kalkulator;
import com.example.kalkulatorsederhana.service.KalkulatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class KalkulatorController {
    Kalkulator kalkulator = new Kalkulator();

    @Autowired
    private KalkulatorService service;

    @RequestMapping("/")
    public String getKalkulator(Model model){
        model.addAttribute("kalkulator", kalkulator);
        return "index";
    }

    @RequestMapping(value = "/", params = "tambah", method = RequestMethod.POST)
    public String tambah(@ModelAttribute("kalkulator") Kalkulator kalkulator, Model model){
        model.addAttribute("hasil", service.tambah(kalkulator));
        return "index";
    }

    @RequestMapping(value = "/", params = "kurang", method = RequestMethod.POST)
    public String kurang(@ModelAttribute("kalkulator") Kalkulator kalkulator, Model model){
        model.addAttribute("hasil", service.kurang(kalkulator));
        return "index";
    }

    @RequestMapping(value = "/", params = "kali", method = RequestMethod.POST)
    public String kali(@ModelAttribute("kalkulator") Kalkulator kalkulator, Model model){
        model.addAttribute("hasil", service.kali(kalkulator));
        return "index";
    }

    @RequestMapping(value = "/", params = "bagi", method = RequestMethod.POST)
    public String bagi(@ModelAttribute("kalkulator") Kalkulator kalkulator, Model model){
        model.addAttribute("hasil", service.bagi(kalkulator));
        return "index";
    }
}
