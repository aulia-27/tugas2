package com.example.kalkulatorsederhana.model;

public class Kalkulator {
    private int a,b;

    public Kalkulator() {
    }

    public Kalkulator(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
