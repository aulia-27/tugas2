package com.example.kalkulatorsederhana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KalkulatorSederhanaApplication {

    public static void main(String[] args) {
        SpringApplication.run(KalkulatorSederhanaApplication.class, args);
    }

}
